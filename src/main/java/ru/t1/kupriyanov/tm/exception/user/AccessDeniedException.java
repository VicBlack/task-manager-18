package ru.t1.kupriyanov.tm.exception.user;

public class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! Access is denied!");
    }

}
